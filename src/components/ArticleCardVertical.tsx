import { Avatar, Box, Card, Group, Image, Text } from '@mantine/core';

interface ArticleCardVerticalProps {
  image: string;
  category: string;
  title: string;
  date: string;
  author: {
    name: string;
    avatar: string;
  };
}

function ArticleCardVertical({
  image,
  category,
  title,
  date,
  author,
}: ArticleCardVerticalProps) {
  return (
    <Card
      w="100%"
      withBorder
      radius="md"
      p={0}
      sx={(theme) => ({ backgroundColor: theme.colors.dark[7] })}
    >
      <Group noWrap spacing={0}>
        <Image src={image} height={140} width={140} />
        <Box p="md">
          <Text transform="uppercase" color="dimmed" weight={700} size="xs">
            {category}
          </Text>
          <Text
            sx={(theme) => ({
              fontFamily: `Greycliff CF, ${theme.fontFamily}`,
            })}
            fw={700}
            lh={1.2}
            mt="xs"
            mb="md"
          >
            {title}
          </Text>
          <Group noWrap spacing="xs">
            <Group spacing="xs" noWrap>
              <Avatar size={20} src={author.avatar} />
              <Text size="xs">{author.name}</Text>
            </Group>
            <Text size="xs" color="dimmed">
              •
            </Text>
            <Text size="xs" color="dimmed">
              {date}
            </Text>
          </Group>
        </Box>
      </Group>
    </Card>
  );
}

export default ArticleCardVertical;
