import {
  TextInput,
  TextInputProps,
  ActionIcon,
  useMantineTheme,
} from '@mantine/core';
import { Icon } from '@iconify-icon/react';
import { useState } from 'react';

function SearchInput({
  onSearch,
  actionLoading = false,
  ...rest
}: TextInputProps & {
  onSearch: (values: string) => void;
  actionLoading?: boolean;
}) {
  const theme = useMantineTheme();
  const [searchTerm, setSearchTerm] = useState<string>('');

  return (
    <TextInput
      icon={<Icon icon="mdi:magnify" />}
      radius="xl"
      size="md"
      rightSection={
        <ActionIcon
          size={32}
          radius="xl"
          color={theme.primaryColor}
          variant="filled"
          loading={actionLoading}
          onClick={() => {
            onSearch(searchTerm);
          }}
        >
          <Icon icon="ep:right" />
        </ActionIcon>
      }
      value={searchTerm}
      onChange={(term) => {
        setSearchTerm(term.target.value);
      }}
      placeholder="Search song name"
      rightSectionWidth={42}
      {...rest}
    />
  );
}

export default SearchInput;
