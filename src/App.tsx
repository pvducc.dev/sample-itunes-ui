import { MantineProvider } from '@mantine/core';
import Home from './pages';

function App() {
  return (
    <MantineProvider
      theme={{
        colorScheme: 'dark',
      }}
      withGlobalStyles
      withNormalizeCSS
    >
      <main>
        <Home />
      </main>
    </MantineProvider>
  );
}

export default App;
