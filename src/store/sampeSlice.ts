import { PayloadAction, createSlice } from '@reduxjs/toolkit';

interface ItunesResponse {
  resultCount: number;
  results: {
    kind: string;
    artistId: number;
    artistName: string;
    wrapperType: 'track' | 'audio';
    artistViewUrl: string;
    artworkUrl100: string;
    trackName: string;
    trackTimeMillis: number;
    trackPrice: number;
    releaseDate: 'string';
    primaryGenreName: string;
  }[];
}

type InitState = {
  isLoading: boolean;
  trackList?: ItunesResponse['results'];
};

const initState: InitState = {
  isLoading: false,
};

const sampleSlice = createSlice({
  name: 'Sample',
  initialState: initState,
  reducers: {
    getTrackList: (state) => {
      state.isLoading = true;
    },
    getTrackListSuccess: (state, action: PayloadAction<ItunesResponse>) => {
      (state.isLoading = false), (state.trackList = action.payload.results);
    },
  },
});

// Actions
export const sampleAction = sampleSlice.actions;
// Reducer
const sampleReducer = sampleSlice.reducer;
export default sampleReducer;
