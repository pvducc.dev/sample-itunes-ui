import ArticleCardVertical from '@/components/ArticleCardVertical';
import SearchInput from '@/components/SearchInput';
import { Box, Container, ScrollArea, Stack, Text } from '@mantine/core';
import { useCallback, useState } from 'react';

interface ItunesRes {
  resultCount: number;
  results: {
    kind: string;
    artistId: number;
    artistName: string;
    wrapperType: 'track' | 'audio';
    artistViewUrl: string;
    artworkUrl100: string;
    trackName: string;
    trackTimeMillis: number;
    trackPrice: number;
    releaseDate: 'string';
    primaryGenreName: string;
  }[];
}

const millisToMinutesAndSeconds = (millis: number) => {
  const minutes = Math.floor(millis / 60000);
  const seconds = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ':' + (parseInt(seconds, 10) < 10 ? '0' : '') + seconds;
};

function useGetSong() {
  const [song, setSong] = useState<ItunesRes>();
  const [status, setStatus] = useState<
    'loading' | 'success' | 'error' | 'idle'
  >('idle');

  const fetching = useCallback(
    async (values: string) => {
      if (!values) {
        return;
      }
      if (!song) {
        setSong(undefined);
      }

      setStatus('loading');
      const fetched = await fetch(
        `https://itunes.apple.com/vn/search?term=${values.trim()}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
          mode: 'cors',
        }
      )
        .then((res) => {
          setStatus('success');
          return res.json() as unknown as ItunesRes;
        })
        .catch((error) => {
          setStatus('error');
          console.warn(error);
        });

      if (fetched) {
        setSong(fetched);
      }
    },
    [song]
  );

  return { data: song, status, mutate: fetching };
}

function TrackList({ data }: { data: ItunesRes['results'] }) {
  return (
    <ScrollArea h="100%" my={10}>
      <Stack align="center" justify="center" spacing="sm" w="80%" m="auto">
        {data.map((item, idx) => (
          <ArticleCardVertical
            key={idx}
            author={{
              name: item.artistName || '',
              avatar: item.artworkUrl100 || '',
            }}
            category={item.primaryGenreName || ''}
            date={millisToMinutesAndSeconds(item.trackTimeMillis)}
            image={item.artworkUrl100 || ''}
            title={item.trackName || ''}
          />
        ))}
      </Stack>
    </ScrollArea>
  );
}

function Home() {
  const { data, status, mutate } = useGetSong();

  // const data = useMemo<ItunesRes['results']>(() => {
  //   if (!song) {
  //     return [];
  //   }

  //   return song.results;
  // }, [song]);

  const handleSearch = useCallback(
    async (value: string) => {
      await mutate(value);
    },
    [mutate]
  );

  return (
    <Container
      maw={1200}
      sx={{
        maxWidth: '1200px',
        minHeight: '100vh',
        margin: '0 auto',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Box
        h={500}
        miw="80%"
        sx={(theme) => ({
          backgroundColor: theme.colors.dark[6],
          borderRadius: theme.radius.md,
          overflow: 'hidden',
        })}
      >
        <Box p={5} w="70%" mx="auto" mt="xl">
          <SearchInput
            actionLoading={status === 'loading'}
            onSearch={(values) => {
              handleSearch(values);
            }}
          />
        </Box>
        {data && data.results.length > 0 && status === 'success' ? (
          <TrackList data={data.results} />
        ) : (
          <Text>Cannot find data</Text>
        )}
      </Box>
    </Container>
  );
}

export default Home;
